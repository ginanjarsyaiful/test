import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environment';
import { Post } from '../_models';

@Injectable({ providedIn: 'root' })
export class PostService {

    constructor(
        private http: HttpClient
    ) {
    }

    getPosts() {
        return this.http.get<Post[]>(`${environment.apiUrl}/posts`)
            .pipe(map(posts => {
                return posts.map(post =>{
                    return new Post(
                        post.id,
                        post.userId,
                        post.title,
                        post.body,
                        0
                    );

                });
            }));
    }

    getComments(post_id):Promise<any[]>{
        return new Promise<any[]>((resolve) => {
            
            return this.http.get(`${environment.apiUrl}/posts/${post_id}/comments`).pipe(map((comments:any) => {
                return comments;
            })).subscribe(res=>{
                resolve(res);
            });
          })
          
    }
    
    getPostDetail(post_id){
        
        return this.http.get<Post>(`${environment.apiUrl}/posts/${post_id}`)
            .pipe(map(post => {
                return new Post(
                    post.id,
                    post.userId,
                    post.title,
                    post.body,
                    0
                );
            }));
    }
}