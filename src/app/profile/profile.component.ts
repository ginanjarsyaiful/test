import { Component, OnInit } from '@angular/core';

import { User } from '../_models';
import { AccountService } from '../_services';

@Component({ templateUrl: 'profile.component.html' })
export class ProfileComponent {
    user: User;
    constructor(private accountService: AccountService) {
        this.user = this.accountService.userValue;
        this.doGetProfile(this.user.id);
    }

    doGetProfile(id){

        this.accountService.getProfile(id).subscribe(profile => {
            this.user = profile;
        })

    }

}