import { Component, OnInit } from '@angular/core';

import { Post } from '../_models';
import { PostService } from '../_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit{
    posts: Post[];
    page = 1;
    constructor(private postService: PostService) {
    }

    ngOnInit() {
        this.doGetPost();
    }

    doGetPost(){
        this.postService.getPosts().subscribe((posts) => {
            console.log(posts);
            this.posts = posts;
        })
    }
}