import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Post } from '../_models';
import { PostService } from '../_services';

@Component({ templateUrl: 'detail.component.html' })
export class DetailComponent implements OnInit{
    post: Post;
    page = 1;
    comments = [];
    constructor(private postService: PostService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        const id = this.route.snapshot.params.id;
        this.doGetDetailPost(id);
        this.doGetComments(id);

    }

    doGetDetailPost(id){
        this.postService.getPostDetail(id).subscribe((post) => {
            this.post = post;
        })
    }

    doGetComments(id){

        this.postService.getComments(id).then(comments=>{
            this.comments = comments;
         });
    }
}