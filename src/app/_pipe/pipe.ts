import { Pipe, PipeTransform } from '@angular/core';
import { PostService } from '../_services';

@Pipe({name: 'comment'})
export class CommentPipe implements PipeTransform {
    result: number = 0;
    constructor(private postService:PostService){

    }
    transform(id: number) {
        return this.postService.getComments(id).then(comments=>{
           return comments.length;
        });
    }
}