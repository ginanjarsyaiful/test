# Test - Syaiful Ginanjar

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.3.

## Install dependency before run the application

Run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

